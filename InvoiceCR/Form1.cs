﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InvoiceCR
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                lblResultados.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtCertificado.Text = openFileDialog1.FileName;
            }

            //Se leen los bytes del archivo del certificado
            Service.Service1Client client = new Service.Service1Client();
           
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtXML.Text = openFileDialog1.FileName;
            }

        }

        private void btnToken_Click(object sender, EventArgs e)
        {
            lblToken.Text = "";
            Service.Service1Client client = new Service.Service1Client();
            //Los ultimos dos parametros son los que identifican con la API
            lblToken.Text = client.GetTokenHacienda(txtUsuarioHacienda.Text, txtPasswordHacienda.Text, "admin", "admin");

        }

        private void btnEnvio_Click(object sender, EventArgs e)
        {

            Service.Service1Client client = new Service.Service1Client();
            //Se cargan los certificados en un Arreglo de Bytes
            byte[] bytesCertificado = File.ReadAllBytes(txtCertificado.Text);

            //Se carga el XML
            String XML = File.ReadAllText(txtXML.Text);
            
            txtClave.Text = client.SendXMLHacienda(lblToken.Text, XML, bytesCertificado, txtClaveCertificado.Text, "admin", "admin");


        }

        private void btnConsulta_Click(object sender, EventArgs e)
        {

            string path = @lblResultados.Text+"\\"+txtClave.Text+".xml";

            Service.Service1Client client = new Service.Service1Client();
            String respuesta = client.ConsultarDocumentoEnviado(txtClave.Text, lblToken.Text, "admin", "admin");

            string createText = respuesta;
            File.WriteAllText(path, createText);
        }
    }
}
